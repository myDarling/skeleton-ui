# skeleton-ui

### 关于框架

skeleton-ui 是由 河浪 个人开发的 专用于 uni-app 生态的 骨架屏效果框架。致力于以简单便捷的方式实现 uni-app 生态应用的骨架屏加载效果。

### 框架安装

[语雀 skeleton-ui - 下载&配置&更新](https://www.yuque.com/he_lang/skeleton-ui/cy4ehn)

### 文档链接

[语雀 skeleton-ui](https://www.yuque.com/he_lang/skeleton-ui)

### 在线体验

![H5平台二维码](https://mydarling.gitee.io/skeleton-ui/static/H5-QR-Code.png)